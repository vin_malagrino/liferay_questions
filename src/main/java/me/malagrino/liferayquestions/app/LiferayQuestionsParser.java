package me.malagrino.liferayquestions.app;

import java.util.*;
import static java.util.stream.Collectors.*;
import java.io.*;

public class LiferayQuestionsParser {
	public static List<Question> parse(BufferedReader questionsReader) {
		List<Question> questions = new ArrayList<>();
		
		
		List<String> lines = questionsReader.lines().collect(toList());
		
		int i = 0;
		while(i < lines.size()) {
			String line = lines.get(i);
		
			Question question = new Question(line);
		
			while(true) {
				i++;
				line = lines.get(i);
			
				if(line.startsWith("Answer: ")) {
					String[] correctAnswers = line.substring(line.indexOf(" ")+1).split(",");
					for(String answer: correctAnswers) {
						question.addCorrectAnswers(answer);
					}
					break;
				}
				else {
					int dotPos = line.indexOf(".");
					String num = line.substring(0, dotPos);
					String opt = line.substring(dotPos+2);
				
					question.addAnswer(num, opt);
				}
			}
		
			questions.add(question);
			i++;
		}
		
		return questions;
	}
}
				
			
