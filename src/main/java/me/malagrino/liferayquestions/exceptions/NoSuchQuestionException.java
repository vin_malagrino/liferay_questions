package me.malagrino.liferayquestions.exceptions;

public class NoSuchQuestionException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public NoSuchQuestionException() {
		super();
	}

	public NoSuchQuestionException(String message) {
		super(message);
	}
}
